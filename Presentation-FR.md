# [Android - Pix]

Pix, le site internet (disponible à https://pix.watch/) est un hébergeur FOSS et chiffré d'images, en libre accès et anonymisé.

Pix, l'application android, est le développement d'une petite application dont le but est de fournir une interface pour rapidement et automatiquement envoyer une image sur cet hébergeur, avec quelques features supplémentaires.

- UI pour sélectionner puis envoyer une image
- Action dans le menu "share" d'Android, pour rapidement envoyer une image
- Stockage des liens des dernières images mises en ligne (avec possibilité de vider ce stockage)
- Quelques réglages permettant notamment de choisir où envoyer l'image, permettant aux utilisateurs hébergeant leur propre instance de Pix d'aussi utiliser ce client.

Le but final est de fournir l'apk en FOSS, dans le marché F-Droid, et potentiellement à la fois sur un site (qui sera dédié à la présentation de Pix) et sur le Google Play Store.

Le dépôt est disponible à https://gitlab.com/Artemix/Pix
