# Pix

Android app to interact with Pix (image hosting service, https://pix.watch/).

## Planned features

- Uploading an image from within the app
- Uploading an image by the "share" shortcut in Android
- Listing latest uploads
- Clear uploads list
